package com.tiggerj.opengl_study.objects;

import com.tiggerj.opengl_study.Constants;
import com.tiggerj.opengl_study.data.VertexArray;
import com.tiggerj.opengl_study.programs.TextureShaderProgram;

import static android.opengl.GLES20.GL_TRIANGLE_FAN;
import static android.opengl.GLES20.glDrawArrays;

/**
 * Created by BJ on 2017. 1. 3..
 */

public class RowResTable {
    private static final int POSITION_COMPONENT_COUNT = 2;
    private static final int TEXTURE_COORDINATES_COMPONENT_COUNT = 2;
    private static final int STRIDE = (POSITION_COMPONENT_COUNT + TEXTURE_COORDINATES_COMPONENT_COUNT) * Constants.BYTES_PER_FLOAT;

    private static final float[] VERTEX_DATA = {
            // Order of coordinates: X, Y, S, T
            // Triangle Fan
            0f, 0f, 0.5f, 0f,
            -0.1f, -0.2f, 0f, 1f,
            0.1f, -0.2f, 1f, 1f,
            0.1f, 0.2f, 1f, 0f,
            -0.1f, 0.2f, 0f, 0f,
            -0.1f, -0.2f, 0f, 1f};

    private final VertexArray vertexArray;

    public RowResTable() {
        vertexArray = new VertexArray(VERTEX_DATA);
    }

    public void bindData(TextureShaderProgram textureProgram) {
        vertexArray.setVertexAttribPointer(0, textureProgram.getPositionAttributeLocation(), POSITION_COMPONENT_COUNT, STRIDE);
        vertexArray.setVertexAttribPointer(POSITION_COMPONENT_COUNT, textureProgram.getTextureCoordinatesAttributeLocation(), TEXTURE_COORDINATES_COMPONENT_COUNT, STRIDE);
    }

    public void draw() {
        glDrawArrays(GL_TRIANGLE_FAN, 0, 6);
    }
}
