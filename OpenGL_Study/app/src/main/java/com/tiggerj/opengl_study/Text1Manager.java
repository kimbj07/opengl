package com.tiggerj.opengl_study;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;


/**
 * Created by BJ on 2016. 10. 26..
 */
public class Text1Manager {
    private static final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "attribute vec4 aColor;" +
                    "attribute vec2 aTextCoord;" +
                    "varying vec4 vColor;" +
                    "varying vec2 vTextCoord;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "  vTextCoord = aTextCoord;" +
                    "  vColor = aColor;" +
                    "}";

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "varying vec4 vColor;" +
                    "varying vec2 vTextCoord;" +
                    "uniform sampler2D sTexture;" +
                    "void main() {" +
                    "  gl_FragColor = texture2D( sTexture, vTextCoord ) * vColor;" +
                    "  gl_FragColor.rgb *= vColor.a;" +
                    "}";

    private final int[] LETTER_SIZES = { //
            36, 29, 30, 34, 25, 25, 34, 33,
            11, 20, 31, 24, 48, 35, 39, 29,
            42, 31, 27, 31, 34, 35, 53, 35,
            31, 27, 30, 26, 28, 26, 31, 28,
            28, 28, 29, 29, 14, 24, 30, 18,
            26, 14, 14, 14, 25, 28, 31, 0,
            0, 38, 39, 12, 36, 34, 0, 0,
            0, 38, 0, 0, 0, 0, 0, 0};

    private final float TEXT_UV_BOX_WIDTH = 0.125f; // 1 / length of character => 1 / 8 = 0.125
    private final float TEXT_WIDTH = 64.0f;
    private final float TEXT_SPACESIZE = 40f;

    private final int mProgram;

    public Vector<Text1Object> textCollection;

    private FloatBuffer vertexBuffer;
    private FloatBuffer textureBuffer;
    private FloatBuffer colorBuffer;
    private ShortBuffer drawListBuffer;

    private float[] vecs;
    private float[] uvs;
    private short[] indices;
    private float[] colors;

    private int indexVecs;
    private int indexIndices;
    private int indexUvs;
    private int indexColors;

    private int mTextureId;

    private float mUniformScale;

    public Text1Manager() {
        textCollection = new Vector<>();

        vecs = new float[3 * 10];
        colors = new float[4 * 10];
        uvs = new float[2 * 10];
        indices = new short[10];

        int vertexShader = Text1GLRenderer.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = Text1GLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);
    }

    public void addTextObject(Text1Object textObject) {
        textCollection.add(textObject);
    }

    public void setTextureId(int textureId) {
        mTextureId = textureId;
    }

    public void prepareDraw() {
        indexVecs = 0;
        indexIndices = 0;
        indexUvs = 0;
        indexColors = 0;

        int charCount = 0;
        for (Text1Object textObject : textCollection) {
            if (textObject != null && textObject.text != null) {
                charCount += textObject.text.length();
            }
        }

        vecs = new float[charCount * 12];
        colors = new float[charCount * 16];
        uvs = new float[charCount * 8];
        indices = new short[charCount * 6];

        for (Text1Object textObject : textCollection) {
            if (textObject != null && textObject.text != null) {
                convertTextToTriangleInfo(textObject);
            }
        }
    }

    public void addCharRenderInformation(float[] vec, float[] cs, float[] uv, short[] indices) {
        // We need a base value because the object has indices related to
        // that object and not to this collection so basicly we need to
        // translate the indices to align with the vertexlocation in ou
        // vecs array of vectors.
        short base = (short) (indexVecs / 3);

        // We should add the vec, translating the indices to our saved vector
        for (int i = 0; i < vec.length; i++) {
            vecs[indexVecs] = vec[i];
            indexVecs++;
        }

        // We should add the colors.
        for (int i = 0; i < cs.length; i++) {
            colors[indexColors] = cs[i];
            indexColors++;
        }

        // We should add the uvs
        for (int i = 0; i < uv.length; i++) {
            uvs[indexUvs] = uv[i];
            indexUvs++;
        }

        // We handle the indices
        for (int j = 0; j < indices.length; j++) {
            this.indices[indexIndices] = (short) (base + indices[j]);
            indexIndices++;
        }
    }

    public void draw(float[] mvpMatrix) {
        // Set the correct shader for our grid object.
        GLES20.glUseProgram(mProgram);

        // The vertex buffer.
        ByteBuffer bb = ByteBuffer.allocateDirect(vecs.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(vecs);
        vertexBuffer.position(0);

        // The vertex buffer.
        ByteBuffer bb3 = ByteBuffer.allocateDirect(colors.length * 4);
        bb3.order(ByteOrder.nativeOrder());
        colorBuffer = bb3.asFloatBuffer();
        colorBuffer.put(colors);
        colorBuffer.position(0);

        // The texture buffer
        ByteBuffer bb2 = ByteBuffer.allocateDirect(uvs.length * 4);
        bb2.order(ByteOrder.nativeOrder());
        textureBuffer = bb2.asFloatBuffer();
        textureBuffer.put(uvs);
        textureBuffer.position(0);

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(indices.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(indices);
        drawListBuffer.position(0);

        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the background coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);

        int mTexCoordLoc = GLES20.glGetAttribLocation(mProgram, "aTextCoord");

        // Prepare the texturecoordinates
        GLES20.glVertexAttribPointer(mTexCoordLoc, 2, GLES20.GL_FLOAT, false, 0, textureBuffer);

        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glEnableVertexAttribArray(mTexCoordLoc);

        int mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mColorHandle);

        // Prepare the background coordinate data
        GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false, 0, colorBuffer);

        // get handle to shape's transformation matrix
        int mtrxhandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, mvpMatrix, 0);

        int mSamplerLoc = GLES20.glGetUniformLocation(mProgram, "sTexture");

        // Set the sampler texture unit to our selected id
        GLES20.glUniform1i(mSamplerLoc, mTextureId);

        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indices.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTexCoordLoc);
        GLES20.glDisableVertexAttribArray(mColorHandle);
    }

    private int convertCharToIndex(int input) {
        int index = -1;

        // Retrieve the index
        if (input > 64 && input < 91) { // A-Z
            index = input - 65;
        } else if (input > 96 && input < 123) { // a-z
            index = input - 97;
        } else if (input > 47 && input < 58) { // 0-9
            index = input - 48 + 26;
        } else if (input == 43) { // +
            index = 38;
        } else if (input == 45) { // -
            index = 39;
        } else if (input == 33) { // !
            index = 36;
        } else if (input == 63) { // ?
            index = 37;
        } else if (input == 61) { // =
            index = 40;
        } else if (input == 58) { // :
            index = 41;
        } else if (input == 46) { // .
            index = 42;
        } else if (input == 44) { // ,
            index = 43;
        } else if (input == 42) { // *
            index = 44;
        } else if (input == 36) { // $
            index = 45;
        }

        return index;
    }

    private void convertTextToTriangleInfo(Text1Object textObject) {
        float x = textObject.x;
        float y = textObject.y;
        String text = textObject.text;

        for (int j = 0; j < text.length(); j++) {
            // ASCII 값 추출
            int index = convertCharToIndex(text.charAt(j));
            if (index == -1) {
                // unknown character, we will add a space for it to be save.
                x += ((TEXT_SPACESIZE) * mUniformScale);
                continue;
            }

            // Calculate the uv parts
            int row = index / 8;
            int col = index % 8;

            float v = row * TEXT_UV_BOX_WIDTH;
            float v2 = v + TEXT_UV_BOX_WIDTH;
            float u = col * TEXT_UV_BOX_WIDTH;
            float u2 = u + TEXT_UV_BOX_WIDTH;

            // Creating the triangle information
            float[] vec = new float[12];
            float[] uv = new float[8];
            float[] colors;

            vec[0] = x;
            vec[1] = y + (TEXT_WIDTH * mUniformScale);
            vec[2] = 0.99f;
            vec[3] = x;
            vec[4] = y;
            vec[5] = 0.99f;
            vec[6] = x + (TEXT_WIDTH * mUniformScale);
            vec[7] = y;
            vec[8] = 0.99f;
            vec[9] = x + (TEXT_WIDTH * mUniformScale);
            vec[10] = y + (TEXT_WIDTH * mUniformScale);
            vec[11] = 0.99f;

            colors = new float[]{
                    textObject.color[0], textObject.color[1], textObject.color[2], textObject.color[3],
                    textObject.color[0], textObject.color[1], textObject.color[2], textObject.color[3],
                    textObject.color[0], textObject.color[1], textObject.color[2], textObject.color[3],
                    textObject.color[0], textObject.color[1], textObject.color[2], textObject.color[3]
            };

            // 0.001f = texture bleeding hack/fix
            uv[0] = u + 0.001f;
            uv[1] = v + 0.001f;
            uv[2] = u + 0.001f;
            uv[3] = v2 - 0.001f;
            uv[4] = u2 - 0.001f;
            uv[5] = v2 - 0.001f;
            uv[6] = u2 - 0.001f;
            uv[7] = v + 0.001f;

            short[] indices = {0, 1, 2, 0, 2, 3};

            // Add our triangle information to our collection for 1 render call.
            addCharRenderInformation(vec, colors, uv, indices);

            // Calculate the new position
            x += ((LETTER_SIZES[index]) * mUniformScale);
        }
    }

    public void setUniformScale(float mUniformScale) {
        this.mUniformScale = mUniformScale;
    }
}