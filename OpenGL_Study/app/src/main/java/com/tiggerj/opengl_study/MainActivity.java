package com.tiggerj.opengl_study;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View triangle = findViewById(R.id.triangle);
        triangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TrangleActivity.class));
            }
        });

        View helloWorld1 = findViewById(R.id.helloworld1);
        helloWorld1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Text1Activity.class));
            }
        });

        View helloWorld2 = findViewById(R.id.helloworld2);
        helloWorld2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Text1Activity.class));
            }
        });

        View lession1 = findViewById(R.id.lesson1);
        lession1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Lesson1Activity.class));
            }
        });

        View firstOpenGLProject = findViewById(R.id.firstOpenGLProject);
        firstOpenGLProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FirstOpenGLProjectActivity.class));
            }
        });

        View airHockeySimple = findViewById(R.id.airHockeySimple);
        airHockeySimple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockeySimpleActivity.class));
            }
        });

        View airHockeyAdvanced = findViewById(R.id.airHockeyAdvanced);
        airHockeyAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockeyAdvancedActivity.class));
            }
        });

        View airHockeyOrtho = findViewById(R.id.airHockeyOrtho);
        airHockeyOrtho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockeyOrthoActivity.class));
            }
        });

        View airHockey3DWithW = findViewById(R.id.airHockey3DWithW);
        airHockey3DWithW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockey3DWithWActivity.class));
            }
        });

        View airHockey3D = findViewById(R.id.airHockey3D);
        airHockey3D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockey3DActivity.class));
            }
        });

        View airHockeyTextured = findViewById(R.id.airHockeyTextured);
        airHockeyTextured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockeyTexturedActivity.class));
            }
        });

        View airHockeyWithBetterMallets = findViewById(R.id.airHockeyWithBetterMallets);
        airHockeyWithBetterMallets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AirHockeyWithBetterMalletsActivity.class));
            }
        });
    }
}