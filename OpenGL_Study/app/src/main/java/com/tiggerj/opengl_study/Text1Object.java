package com.tiggerj.opengl_study;

public class Text1Object {

    public String text;
    public float x;
    public float y;
    public float[] color;

    public Text1Object() {
        text = "default";
        x = 0f;
        y = 0f;
        color = new float[]{1f, 1f, 1f, 1.0f};
    }

    public Text1Object(String txt, float xcoord, float ycoord) {
        text = txt;
        x = xcoord;
        y = ycoord;
        color = new float[]{1f, 1f, 1f, 1.0f};
    }
}