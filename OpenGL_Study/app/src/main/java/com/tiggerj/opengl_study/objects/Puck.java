package com.tiggerj.opengl_study.objects;

import com.tiggerj.opengl_study.data.VertexArray;
import com.tiggerj.opengl_study.programs.BetterMalletColorShaderProgram;
import com.tiggerj.opengl_study.util.Geometry;

import java.util.List;

/**
 * Created by BJ on 2017. 1. 5..
 */

public class Puck {
    private static final int POSITION_COMPONENT_COUNT = 3;

    public final float radius, height;
    private final VertexArray vertexArray;
    private final List<ObjectBuilder.DrawCommand> drawCommands;

    public Puck(float radius, float height, int numPointsAroundPuck) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createPuck(new Geometry.Cylinder(new Geometry.Point(0f, 0f, 0f), radius, height), numPointsAroundPuck);
        this.radius = radius;
        this.height = height;
        vertexArray = new VertexArray(generatedData.vertexData);
        drawCommands = generatedData.drawList;
    }

    public void bindData(BetterMalletColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(0, colorProgram.getPositionAttributeLocation(), POSITION_COMPONENT_COUNT, 0);
    }

    public void draw() {
        for (ObjectBuilder.DrawCommand drawCommand : drawCommands) {
            drawCommand.draw();
        }
    }
}
