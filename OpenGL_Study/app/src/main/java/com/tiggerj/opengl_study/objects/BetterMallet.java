package com.tiggerj.opengl_study.objects;

import com.tiggerj.opengl_study.data.VertexArray;
import com.tiggerj.opengl_study.programs.BetterMalletColorShaderProgram;
import com.tiggerj.opengl_study.util.Geometry;

import java.util.List;

/**
 * Created by BJ on 2017. 1. 3..
 */

public class BetterMallet {
    private static final int POSITION_COMPONENT_COUNT = 3;
    public final float radius;
    public final float height;
    private final VertexArray vertexArray;
    private final List<ObjectBuilder.DrawCommand> drawCommands;

    public BetterMallet(float radius, float height, int numPointsAroundMallet) {
        ObjectBuilder.GeneratedData generatedData = ObjectBuilder.createMallet(new Geometry.Point(0f, 0f, 0f), radius, height, numPointsAroundMallet);
        this.radius = radius;
        this.height = height;
        vertexArray = new VertexArray(generatedData.vertexData);
        drawCommands = generatedData.drawList;
    }

    public void bindData(BetterMalletColorShaderProgram colorProgram) {
        vertexArray.setVertexAttribPointer(0, colorProgram.getPositionAttributeLocation(), POSITION_COMPONENT_COUNT, 0);
    }

    public void draw() {
        for (ObjectBuilder.DrawCommand drawCommand : drawCommands) {
            drawCommand.draw();
        }
    }
}
