package com.tiggerj.opengl_study;

import android.graphics.PointF;
import android.graphics.RectF;

public class Sprite {
    private float mAngle;
    private float mScale;
    private RectF mBase;
    private PointF mTranslation;

    public Sprite(float ssu) {
        // Initialise our intital size around the 0,0 point
        mBase = new RectF(-50f * ssu, 50f * ssu, 50f * ssu, -50f * ssu);

        // Initial mTranslation
        mTranslation = new PointF(50f * ssu, 50f * ssu);

        // We start with our inital size
        mScale = 1f;

        // We start in our inital mAngle
        mAngle = 0f;
    }


    public void translate(float deltax, float deltay) {
        // Update our location.
        mTranslation.x += deltax;
        mTranslation.y += deltay;
    }

    public void scale(float deltas) {
        mScale += deltas;
    }

    public void rotate(float deltaa) {
        mAngle += deltaa;
    }

    public float[] getTransformedVertices() {
        // Start with scaling
        float x1 = mBase.left * mScale;
        float x2 = mBase.right * mScale;
        float y1 = mBase.bottom * mScale;
        float y2 = mBase.top * mScale;

        // We now detach from our Rect because when rotating,
        // we need the seperate points, so we do so in opengl order
        PointF one = new PointF(x1, y2);
        PointF two = new PointF(x1, y1);
        PointF three = new PointF(x2, y1);
        PointF four = new PointF(x2, y2);

        // We create the sin and cos function once,
        // so we do not have calculate them each time.
        float s = (float) Math.sin(mAngle);
        float c = (float) Math.cos(mAngle);

        // Then we rotate each point
        one.x = x1 * c - y2 * s;
        one.y = x1 * s + y2 * c;
        two.x = x1 * c - y1 * s;
        two.y = x1 * s + y1 * c;
        three.x = x2 * c - y1 * s;
        three.y = x2 * s + y1 * c;
        four.x = x2 * c - y2 * s;
        four.y = x2 * s + y2 * c;

        // Finally we translate the sprite to its correct position.
        one.x += mTranslation.x;
        one.y += mTranslation.y;
        two.x += mTranslation.x;
        two.y += mTranslation.y;
        three.x += mTranslation.x;
        three.y += mTranslation.y;
        four.x += mTranslation.x;
        four.y += mTranslation.y;

        // We now return our float array of vertices.
        return new float[]{
                one.x, one.y, 0.0f,
                two.x, two.y, 0.0f,
                three.x, three.y, 0.0f,
                four.x, four.y, 0.0f,
        };
    }
}