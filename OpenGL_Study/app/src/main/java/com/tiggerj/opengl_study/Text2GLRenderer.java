package com.tiggerj.opengl_study;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class Text2GLRenderer implements GLSurfaceView.Renderer {
    // Our matrices
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private final float[] mMVPMatrix = new float[16];

    private float ssu = 3.0f;

    private Context mContext;

    private Text1Manager mTextManager;

    public Text2GLRenderer(Context context) {
        mContext = context;
    }

    public static int loadShader(int type, String shaderCode) {
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        // return the shader
        return shader;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1);

        int[] textureNames = new int[1];
        GLES20.glGenTextures(1, textureNames, 0);

        int id = mContext.getResources().getIdentifier("drawable/font", null, mContext.getPackageName());

        Bitmap bmp = BitmapFactory.decodeResource(mContext.getResources(), id);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

        bmp.recycle();

        initTextManager();
    }


    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, 1.0f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        mTextManager.draw(mMVPMatrix);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        Matrix.orthoM(mProjectionMatrix, 0, 0f, width, 0f, height, 0, 50);

//        float ratio = (float) width / height;

//         this projection matrix is applied to object coordinates in the onDrawFrame() method
//        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
    }

    public void initTextManager() {
        // Create our text manager
        mTextManager = new Text1Manager();

        // Tell our text manager to use index 1 of textures loaded
        mTextManager.setTextureId(0);

        // Pass the uniform scale
        mTextManager.setUniformScale(ssu);

        // Add it to our manager
        mTextManager.addTextObject(new Text1Object("hello world", 0.5f, 0.5f));

        // Prepare the text for rendering
        mTextManager.prepareDraw();
    }
}
